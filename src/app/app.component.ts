import { Component } from '@angular/core';
import { VERSION } from '@angular/material';

import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

import { LibService } from './lib.service';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router, private bookSvc: LibService, private httpClient: HttpClient) { }

  name = 'AJ\'s Library Search';
  search = ''; // 

  searchType = '(by Author)';
  clue: string = '<firstname>space<lastname>';
  toggleFire: string = '/search';

  /* constructor(private router: Router, private bookSvc: LibService) { } */

  private findBooks(searchType: string, searchStr: string) {
    this.bookSvc.cursor = 0;
    if (searchType == '(by Author)') { this.bookSvc.title = ''; this.bookSvc.name = searchStr; }
    else { this.bookSvc.title = searchStr; this.bookSvc.name = ''; }
    this.search = searchStr;
    this.bookSvc.showcaseTitle = 'SEARCHED BOOKS...';
    console.log('>> lib.title:', this.bookSvc.title);
    console.log('>> lib.name:', this.bookSvc.name);
    console.log('>> searchType:', searchType);
    console.log('>> searchStr:', searchStr);
    console.log('>> lib.name in books:', this.bookSvc.name); (this.toggleFire == '/booksearch') ? this.toggleFire = '/search' : this.toggleFire = '/booksearch';
    this.router.navigate([this.toggleFire]);
  }

  private backHome(searchStr: string) {
    this.bookSvc.cursor = 0;
    searchStr = '';
    this.search = searchStr;
    this.bookSvc.showcaseTitle = 'BROWSE BOOKS';
    this.bookSvc.name = '';
    this.bookSvc.title = '';
    this.router.navigate(['/books']);
  }




}

