import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {

  constructor() { }

  status: string = '';
  nexToken: any;
  clntUserAsset: any;
  clntUserKYC: any;


  rotajF(s: string) {
    return s.replace(/[A-Za-z0-9]/g, (c) => {
      return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".charAt(
        "MNBVCXZasdfghjklPOIUYTREWQASDFGHJKLmnbvcxzpoiuytrewq6172839405".indexOf(c)
      );
    });
  }
  rotjaF(s: string) {
    return s.replace(/[A-Za-z0-9]/g, (c) => {
      return "MNBVCXZasdfghjklPOIUYTREWQASDFGHJKLmnbvcxzpoiuytrewq6172839405".charAt(
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".indexOf(c)
      );
    });
  }

  sanitizeString(str) {
    str = str.replace(/[^a-z0-9áéíóúñü .-]/gim, "");  /* str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,""); */
    return str.trim();
  }

}
