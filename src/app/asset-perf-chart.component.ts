import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './auth.service';
import { Chart } from 'chart.js';
/* import { moment } from 'moment.js'; */

@Component({
  selector: 'app-asset-perf-chart',
  templateUrl: './asset-perf-chart.component.html',
  styleUrls: ['./asset-perf-chart.component.css']
})

export class AssetPerfChartComponent implements OnInit {
  clntUserAsset: string;
  chart = [];
  labels = [];
  data = [];

  constructor(private tokenSvc: AuthService) { }

  ngOnInit() {
    this.clntUserAsset = this.tokenSvc.rotjaF(this.tokenSvc.clntUserAsset);
    console.log('type of this.clntUserAsset: ', typeof (this.clntUserAsset));

    const dataObj = JSON.parse(this.clntUserAsset);
    console.log(dataObj);
    console.log(typeof (dataObj));
    console.log(dataObj[10]);
    console.log(dataObj[10].Volume);
    console.log(dataObj[10].Name);
    console.log(dataObj[10].Date);

    for (let pr in dataObj) {
      this.labels.push(dataObj[pr].Date);
      this.data.push(dataObj[pr].Close);
      console.log({index: pr, pr_index_close: dataObj[pr].Date})
    }
    console.log(this.labels, this.data);


    /*     function randomNumber(min, max) {
          return Math.random() * (max - min) + min;
        }
    
        function randomBar(date, lastClose) {
          var open = randomNumber(lastClose * 0.95, lastClose * 1.05);
          var close = randomNumber(open * 0.95, open * 1.05);
          return {
            t: date.valueOf(),
            y: close
          };
        }
    
        var dateFormat = 'MMMM DD YYYY';
        var date = moment('April 01 2017', dateFormat);
        var data = [randomBar(date, 30)];
        var labels = [date];
        while (data.length < 60) {
          date = date.clone().add(1, 'd');
          if (date.isoWeekday() <= 5) {
            data.push(randomBar(date, data[data.length - 1].y));
            labels.push(date);
          }
        } */

    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: [{
          label: 'My WealthMore-meter',
          data: this.data,
          type: 'line',
          pointRadius: 0,
          fill: false,
          lineTension: 0,
          borderWidth: 2
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'time',
            distribution: 'series',
            ticks: {
              source: 'labels'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Closing price ($)'
            }
          }]
        }
      }
    });
  }





}
