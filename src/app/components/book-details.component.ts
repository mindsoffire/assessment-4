import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LibService } from '../lib.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit, OnDestroy {

  book = {};
  bookId = 0;

  private bookId$: Subscription;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private bookSvc: LibService) { }

  ngOnInit() {
    console.log("BookDetails ngOnInit: ")
    //we're taking a snapshot. snapshot do not change
    //this.bookId = parseInt(this.activatedRoute.snapshot.params.bookId);
    this.bookId$ = this.activatedRoute.params.subscribe(
      (param) => {
        console.log('> param  = ', param);
        this.bookId = parseInt(param.bookId);
        this.bookSvc.getBook(this.bookId)
          .then((result) => this.book = result)
          .catch((err) => {
            alert(`Error: ${JSON.stringify(err)}`);
          });
      }
    )
  }

  ngOnDestroy() {
    this.bookId$.unsubscribe();
  }

  prevBook() {
    this.bookId = this.bookId - 1;
    this.router.navigate(['/book', this.bookId])
  }

  nextBook() {
    this.bookId = this.bookId + 1;
    this.router.navigate(['/book', this.bookId])
  }

  goBack() {
    this.router.navigate(['/books']);
  }

}
