import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LibService } from '../lib.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor(private bookSvc: LibService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  offset = 0;
  private limit = 10;
  books = [];
  title = this.bookSvc.title;
  name = this.bookSvc.name;
  


  ngOnInit() {
    console.log('Books ngOnInit')
    this.offset = this.bookSvc.cursor;
    /* this.bookSvc.showcaseTitle = 'BROWSE BOOKS'; */
    this.loadBooks();
  }

  private loadBooks() {
    console.log('>> lib.title in (load)books:', this.bookSvc.title);
    console.log('>> lib.name in (load)books:', this.bookSvc.name);
    this.bookSvc.getAllBooks({ title: this.title, name: this.name, offset: this.offset, limit: this.limit })
      .then((result) => { this.books = result })
      .catch((error) => { console.error(error); });
  }

  prevBooks() {
    this.offset = this.offset - this.limit;
    this.bookSvc.cursor = this.offset;
    this.loadBooks();
  }

  nextBooks() {
    this.offset = this.offset + this.limit;
    this.bookSvc.cursor = this.offset;
    this.loadBooks();
  }

  showDetails(bookId: number) {
    console.log('> bookId: %d', bookId);
    this.router.navigate(['/book', bookId]);
  }
}