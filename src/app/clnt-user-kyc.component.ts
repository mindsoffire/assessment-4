import { Component, OnInit } from '@angular/core';
import { VERSION } from '@angular/material';

import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

import { LibService } from './lib.service';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-clnt-user-kyc',
  templateUrl: './clnt-user-kyc.component.html',
  styleUrls: ['./clnt-user-kyc.component.css']
})
export class ClntUserKycComponent implements OnInit {

  constructor(private router: Router, private bookSvc: LibService, private tokenSvc: AuthService, private httpClient: HttpClient) { }

  ngOnInit() { }

  step = 0;
  setStep(index: number) {
    this.step = index;
  }
  nextStep() {
    this.step++;
  }
  prevStep() {
    this.step--;
  }

  localUrlFront: any[];
  localUrlBack: any[];
  localUrlBank: any[];
  picSrc: string = "";

  showPreviewImageFront(event: any, fileToSend: string) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrlFront = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  showPreviewImageBack(event: any, fileToSend: string) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrlBack = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  showPreviewImageBank(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrlBank = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }


}
